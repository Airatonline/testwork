package com.example.example;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapterMACRSSI extends RecyclerView.Adapter<MyAdapterMACRSSI.MyViewHolder> {

    private ArrayList<BTElements> myData;
    private Context context;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView macText, rssiText;
        private LinearLayout layout;

        MyViewHolder(View v) {
            super(v);
            macText = v.findViewById(R.id.macAndAddition);
            rssiText = v.findViewById(R.id.rssiText);
            layout = v.findViewById(R.id.element_mac_rssi_layout);
        }

    }


    MyAdapterMACRSSI(ArrayList<BTElements> myData, Context context) {
        this.myData = myData;
        this.context = context;
    }

    @NonNull
    public MyAdapterMACRSSI.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_mac_rssi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.macText.setText(myData.get(position).getMac());
        holder.rssiText.setText(String.valueOf(myData.get(position).getRssi()));
        //для нашим маяков добавляем действие по переходу в окно подробностей
        if (myData.get(position).isBeacon()) {
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BeaconActivity.class);
                    intent.putExtra("mac", myData.get(position).getMac());
                    context.startActivity(intent);

                }
            });
            if (myData.get(position).getRssi() > -70) {
                holder.layout.setBackgroundColor(Color.GREEN);
            }else if(myData.get(position).getRssi()<-110){
                holder.layout.setBackgroundColor(Color.RED);
            }
            else {
                int red = (int)((-70.0 - myData.get(position).getRssi()) / 40 * 280);
                int green = (int)((110.0 + myData.get(position).getRssi()) / 40 * 280);
                holder.layout.setBackgroundColor(Color.rgb(red, green, 0));
                Log.d("AAA", red + "|" + green);
            }
        }

    }


    @Override
    public int getItemCount() {
        return myData.size();
    }


}

