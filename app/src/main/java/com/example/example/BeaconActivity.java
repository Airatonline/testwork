package com.example.example;

import android.content.Intent;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;

public class BeaconActivity extends AppCompatActivity implements BeaconConsumer {

    Region region;
    private BeaconManager beaconManager;
    TextView macText, majorText, minorText, rssiText, distanceText;
    String mac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);
        Intent intent = getIntent();
        mac = intent.getStringExtra("mac");

        macText = findViewById(R.id.macText);
        majorText = findViewById(R.id.majorText);
        minorText = findViewById(R.id.minorText);
        rssiText = findViewById(R.id.rssiText);
        distanceText = findViewById(R.id.rangeText);
        macText.setText(mac);
        majorText.setText(R.string.loading);
        minorText.setText(R.string.loading);
        rssiText.setText(R.string.loading);
        distanceText.setText(R.string.loading);
        beaconManager
                = BeaconManager.getInstanceForApplication(this);
        beaconManager.unbind(this);
        region = new Region("regionForMacAddress", mac);

        beaconManager.bind(this);
    }

    protected void onPause() {
        super.onPause();
        beaconManager.unbind(this);
        beaconManager.removeAllRangeNotifiers();
        beaconManager.removeAllMonitorNotifiers();
        Log.d("AAA", "unbinded");
    }

    @Override
    protected void onStart() {
        super.onStart();
        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                System.out.println("ENTER ------------------->");
                try {
                    beaconManager.startRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void didExitRegion(Region region) {
                System.out.println("EXIT----------------------->");
                try {
                    beaconManager.stopRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                System.out.println("I have just switched from seeing/not seeing beacons: " + i);
            }
        });
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                if (collection.size() > 0) {
                    for (Beacon b : collection) {
                        if (!b.getBluetoothAddress().equals(mac)) {
                            return;
                        }
                        Log.d("AAA", b.getBluetoothAddress());
                        macText.setText(getResources().getString(R.string.mac) + b.getBluetoothAddress());
                        majorText.setText(getResources().getString(R.string.major) + b.getId2());
                        minorText.setText(getResources().getString(R.string.minor) + b.getId3());
                        distanceText.setText(getResources().getString(R.string.distance) +
                                String.format("%.2f", b.getDistance()) + getResources().getString(R.string.meters));
                        rssiText.setText(getResources().getString(R.string.rssi) + b.getRssi());
                    }
                }
            }
        });
        beaconManager.bind(this);
        try {
            beaconManager.startMonitoringBeaconsInRegion(region);
        } catch (RemoteException e) {
        }
    }

    @Override
    public void onBeaconServiceConnect() {

    }
}
