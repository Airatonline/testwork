package com.example.example;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.RemoteException;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class MainActivity extends AppCompatActivity implements BeaconConsumer {
    RecyclerView view;
    int cycle = 0;
    Context context;
    BroadcastReceiver mReceiver;
    TextView loadingText;

    BluetoothAdapter bluetoothAdapter;

    private BeaconManager beaconManager;
    ArrayList<BTElements> beacons = new ArrayList<>();
    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
    boolean isOnlyBeacons;
    final Region region = new Region("myBeaons", null, null, null);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view = findViewById(R.id.recyclerView);
        context = this;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        loadingText = findViewById(R.id.loadingText);
        getPermissions();


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        view.setLayoutManager(linearLayoutManager);
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.unbind(this);
        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                System.out.println("ENTER ------------------->");
                try {
                    beaconManager.startRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void didExitRegion(Region region) {
                System.out.println("EXIT----------------------->");
                try {
                    beaconManager.stopRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                System.out.println("I have just switched from seeing/not seeing beacons: " + i);
            }
        });
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                if (collection.size() > 0) {
                    cycle++;
                    if (loadingText.getVisibility() != View.INVISIBLE) {
                        loadingText.setVisibility(View.INVISIBLE);
                    }
                    //Это очистка элементов, происходт через 5 циклов сканирования,
                    // в простивном случае в списке может указываться не все Beacon'ы
                    if (cycle > 5) {
                        cycle = 0;
                        beacons = new ArrayList<>();
                        if (!isOnlyBeacons) {
                            bluetoothAdapter.startDiscovery();
                            registerReceiver(mReceiver, filter);
                        }
                    }

                    for (Beacon beacon : collection) {
                        Log.d("AAA|Beacon", beacon.getBluetoothAddress());
                        BTElements element;
                        if (isOnlyBeacons) {
                            element = new BTElements(beacon.getBluetoothAddress() + "|" +
                                    beacon.getId2() + "|" + beacon.getId3(), beacon.getRssi(), true);

                        } else {
                            element = new BTElements(beacon.getBluetoothAddress(), beacon.getRssi(), true);
                        }

                        int res = findBeaconElement(beacons, element);
                        if (res != -1) {
                            Log.d("AAA:res", String.valueOf(res));
                            beacons.get(res).setRssi(beacon.getRssi());
                        } else {
                            beacons.add(element);
                        }

                    }
                }
                beacons = sortElements(beacons);
                MyAdapterMACRSSI adapter = new MyAdapterMACRSSI(beacons, context);
                view.setAdapter(adapter);
            }
        });
        beaconManager.bind(this);


    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            Log.e("Exception", Arrays.toString(e.getStackTrace()));
        }
//при тестировании обнарружено, что unbind не выполняется в onDestroy, поэтому я перенес его сюда

        beaconManager.unbind(this);
        Log.d("AAA", "unbinded");
    }

    @Override
    protected void onStart() {
        super.onStart();
        //чтобы обратно запустить сканер, после onPause
        beaconManager.bind(this);
        try {
            beaconManager.startMonitoringBeaconsInRegion(region);
        } catch (Exception e) {
            Log.e("RemoteException", Arrays.toString(e.getStackTrace()));
        }
    }

    int findBeaconElement(ArrayList<BTElements> beacons, BTElements element) {
        for (int i = 0; i < beacons.size(); i++) {
            if (beacons.get(i).getMac().equals(element.getMac())) {
                return i;
            }
        }
        return -1;
    }

    ArrayList<BTElements> sortElements(ArrayList<BTElements> beacons) {

        int lastBeacon = -1;
        if (beacons.size() == 1) {
            return beacons;
        }
        // Collections.sort со своим Comparator работает только с Java 8
        //сортировка по типу Bluetooth устройства
        for (int i = 0; i < beacons.size() - 1; i++) {
            for (int j = 1; j < beacons.size(); j++) {
                if (beacons.get(j).isBeacon() && !beacons.get(j - 1).isBeacon()) {
                    BTElements temp = beacons.get(j);
                    beacons.set(j, beacons.get(j - 1));
                    beacons.set(j - 1, temp);
                }
                if (i == beacons.size() - 2 && !beacons.get(j).isBeacon() && beacons.get(j - 1).isBeacon()) {
                    lastBeacon = j;
                }
            }
        }
        Log.d("AAA|sorted", String.valueOf(lastBeacon));
        if (lastBeacon > -1) {
            //сортировка по уровню сигнала
            for (int i = 0; i < lastBeacon - 1; i++) {
                for (int j = 1; j < lastBeacon; j++) {
                    if (beacons.get(j).getRssi() > beacons.get(j - 1).getRssi()) {
                        BTElements temp = beacons.get(j);
                        beacons.set(j, beacons.get(j - 1));
                        beacons.set(j - 1, temp);
                    }
                }
            }
            for (int i = lastBeacon; i < beacons.size() - 1; i++) {
                for (int j = lastBeacon + 1; j < beacons.size(); j++) {
                    if (beacons.get(j).getRssi() > beacons.get(j - 1).getRssi()) {
                        BTElements temp = beacons.get(j);
                        beacons.set(j, beacons.get(j - 1));
                        beacons.set(j - 1, temp);
                    }
                }
            }
        } else {
            for (int i = 0; i < beacons.size(); i++) {
                for (int j = 1; j < beacons.size() - 1; j++) {
                    if (beacons.get(j).getRssi() > beacons.get(j - 1).getRssi()) {
                        BTElements temp = beacons.get(j);
                        beacons.set(j, beacons.get(j - 1));
                        beacons.set(j - 1, temp);
                    }
                }
            }
        }


        return beacons;
    }

    @Override
    public void onBeaconServiceConnect() {

    }

    public void bluetoothBeacons(final View view1) {
//поиск Bluetooth устройств
        bluetoothAdapter.startDiscovery();
        isOnlyBeacons = false;
        beacons = new ArrayList<>();
        loadingText.setVisibility(View.VISIBLE);

        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    String mac = bluetoothDevice.getAddress();
                    int RSSI = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                    BTElements elements = new BTElements(mac, RSSI, false);
                    int index = findBeaconElement(beacons, elements);
                    if (loadingText.getVisibility() != View.INVISIBLE) {
                        loadingText.setVisibility(View.INVISIBLE);
                    }
                    //ищем данные устройства в списке, дабы избежать дублирования
                    if (index != -1) {
                        beacons.set(index, elements);
                    } else {
                        //если не найдено, добавляем
                        beacons.add(elements);
                    }

                    if(view.getAdapter() == null){
                        beacons = sortElements(beacons);
                        MyAdapterMACRSSI myadapter = new MyAdapterMACRSSI(beacons, context);
                        view.setAdapter(myadapter);
                    }
                }
            }
        };
        registerReceiver(mReceiver, filter);


    }

    public void onlyBeacons(View view) {
        isOnlyBeacons = true;
        loadingText.setVisibility(View.VISIBLE);
        beacons = new ArrayList<>();
    }

    //получаем разрешение на GPS
    void getPermissions() {
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    permissions,
                    1);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            //если не были полуены разрешения, выводим предупрежедние
            Toast.makeText(context, "Warning! App does not work correctly", Toast.LENGTH_LONG).show();
        }
    }
}

class BTElements {
    //формат передачи данных в Адаптер
    BTElements(String mac, int rssi, boolean isBeacon) {
        this.mac = mac;
        this.rssi = rssi;
        this.isBeacon = isBeacon;
    }

    private String mac;
    private int rssi;
    private boolean isBeacon;

    String getMac() {
        return mac;
    }

    int getRssi() {
        return rssi;
    }

    void setRssi(int rssi) {
        this.rssi = rssi;
    }


    boolean isBeacon() {
        return isBeacon;
    }
}