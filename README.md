# BLEFinder

App for find Alt beacons.
Uses AltBeacon library.

## Example

![alt text](https://gitlab.com/Airatonline/testwork/raw/images/example.jpg?inline=false)


## Getting Started

Use command for clone project

```bash
git clone https://gitlab.com/Airatonline/testwork.git
```
When open project by Android Studio

Connect Android device or use Android emulator

Build project on device